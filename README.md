# My dwmblocks build
Modular status bar for dwm written in c.

# Installing

Download the source code from this repository or use a git clone:

	git clone https://gitlab.com/apoorv569/dwmblocks.git
	cd dwmblocks
    sudo make clean install
	
NOTE: Installing this will overwrite your existing dwmblocks installation.

# Modifying dwmblocks
The statusbar is made from text output from scripts found in the statusbar folder. Blocks are added and removed by editing the blocks.h header file.

# Signaling changes

Most statusbars constantly rerun every script every several seconds to update.
This is an option here, but a superior choice is giving your module a signal
that you can signal to it to update on a relevant event, rather than having it
rerun idly.

For example, the audio module has the update signal 10 by default.  Thus,
running `pkill -RTMIN+10 dwmblocks` will update it.

You can also run `kill -44 $(pidof dwmblocks)` which will have the same effect,
but is faster. Just add 34 to your typical signal number.

My volume module *never* updates on its own, instead I have this command run
along side my volume shortcuts in dwm to only update it when relevant.

Note that if you signal an unexpected signal to dwmblocks, it will probably
crash. So if you disable a module, remember to also disable any cronjobs or
other scripts that might signal to that module.

# Clickable modules

This build allows you to build in additional actions into your
scripts in response to click events. See the scripts in the statusbar folder for examples
of this using the `$BLOCK_BUTTON` variable.
