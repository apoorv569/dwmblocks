//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
    /*Icon*/    /*Command*/                                 /*Update Interval*/  /*Update Signal*/
//  {"",        "~/.local/bin/statusbar/music",                      0,                 11},
    {"",        "~/.local/bin/statusbar/weather2",                   1800,            	5},
    {"",        "~/.local/bin/statusbar/delim",                      0,                 13},
    {"",        "~/.local/bin/statusbar/netspeed",                   5,                 4},
    {"",        "~/.local/bin/statusbar/delim",                      0,                 13},
    {"",      "~/.local/bin/statusbar/cpu",                        1,                 18},
    {"",      "~/.local/bin/statusbar/memory",                     10,                14},
    {"",        "~/.local/bin/statusbar/delim",                      0,                 13},
    {"",     	"~/.local/bin/statusbar/cpu-temp",                   5,                 2},
//    {"",     	"~/.local/bin/statusbar/gpu-temp",                   5,                 17},
    {"",        "~/.local/bin/statusbar/delim",                      0,                 13},
//    {" ",      "xbacklight | sed 's/\\..*//'",                      0,                 15},
    {"",      "~/.local/bin/statusbar/brightness",                      0,                 15},
    {"",        "~/.local/bin/statusbar/volume",                     0,                 10},    
    {"",        "~/.local/bin/statusbar/delim",                      0,                 13},
    {"",        "~/.local/bin/statusbar/battery",                    5,                 3},
    {"",        "~/.local/bin/statusbar/delim",                      0,                 13},
//    {"",        "~/.local/bin/statusbar/clock",                      60,                1},
//    {"",        "~/.local/bin/statusbar/delim",                      0,                 13},
    {"",        "~/.local/bin/statusbar/webcam",                     1,                 6},
    {"",        "~/.local/bin/statusbar/microphone",                 1,                 8},
//    {"",        "~/.local/bin/statusbar/delim",                      0,                 13},
    {"",        "~/.local/bin/statusbar/usb",                        1,                 9},
//    {"",        "~/.local/bin/statusbar/delim",                      0,                 13},
    {"",        "~/.local/bin/statusbar/bluetooth",                  1,                 7},
//    {"",        "~/.local/bin/statusbar/delim",                      0,                 13},
    {"",        "~/.local/bin/statusbar/internet",                   1,                 12},
    {"",        "~/.local/bin/statusbar/delim",                      0,                 13},
    {"",        "~/.local/bin/statusbar/pacupdate",                  18000,             16},
    {"",        "~/.local/bin/statusbar/delim",                      0,                 13},
    {"",        "~/.local/bin/statusbar/clock",                      60,                1},
//    {"",        "~/.local/bin/statusbar/delim",                      0,                 13}, 
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = ' ';
